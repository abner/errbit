# -*- encoding: utf-8 -*-
# stub: omniauth-expressov3 1.0.8 ruby lib

Gem::Specification.new do |s|
  s.name = "omniauth-expressov3"
  s.version = "1.0.8"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.3.6") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Abner Oliveira"]
  s.date = "2015-12-09"
  s.description = "Internal authentication handlers for OmniAuth."
  s.email = ["abner.silva@gmail.com"]
  s.files = [".gitignore", "CONTRIBUTING.md", "Gemfile", "Gemfile.lock", "LICENSE", "README.md", "lib/omniauth-expressov3.rb", "lib/omniauth-expressov3/expressov3_auth_client.rb", "lib/omniauth-expressov3/jsonrpc_tine_connection.rb", "lib/omniauth-expressov3/version.rb", "lib/omniauth/strategies/expressov3.rb", "omniauth-expressov3.gemspec", "spec/omniauth-expressov3/expressov3_auth_client_2_spec.rb", "spec/omniauth/strategies/expressov3_spec.rb", "spec/result.json", "spec/spec_helper.rb"]
  s.homepage = "http://github.com/abner/omniauth-expressov3"
  s.rubygems_version = "2.4.6"
  s.summary = "Internal authentication handlers for OmniAuth."

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<omniauth>, ["~> 1.0"])
      s.add_development_dependency(%q<maruku>, ["~> 0.6"])
      s.add_development_dependency(%q<simplecov>, ["~> 0.4"])
      s.add_development_dependency(%q<rack-test>, ["~> 0.5"])
      s.add_development_dependency(%q<rake>, ["~> 0.8"])
      s.add_development_dependency(%q<rspec>, ["~> 2.7"])
    else
      s.add_dependency(%q<omniauth>, ["~> 1.0"])
      s.add_dependency(%q<maruku>, ["~> 0.6"])
      s.add_dependency(%q<simplecov>, ["~> 0.4"])
      s.add_dependency(%q<rack-test>, ["~> 0.5"])
      s.add_dependency(%q<rake>, ["~> 0.8"])
      s.add_dependency(%q<rspec>, ["~> 2.7"])
    end
  else
    s.add_dependency(%q<omniauth>, ["~> 1.0"])
    s.add_dependency(%q<maruku>, ["~> 0.6"])
    s.add_dependency(%q<simplecov>, ["~> 0.4"])
    s.add_dependency(%q<rack-test>, ["~> 0.5"])
    s.add_dependency(%q<rake>, ["~> 0.8"])
    s.add_dependency(%q<rspec>, ["~> 2.7"])
  end
end
